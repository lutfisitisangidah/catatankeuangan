<?php

class Model extends CI_Model {
	function get($tabel) {
		return $this->db->get($tabel);
	}
	function insert_data($data,$table) {
		$this->db->insert($table,$data);
	}
	function hapus($where,$tabel){
		$this->db->delete($tabel,$where);
	}
	function sql($sql) {
		return  $this->db->query($sql);
	}
	function get_satu($table, $kondisi) {
		return $this->db->where($kondisi)->get($table)->row();
	}
	function edit_data($table, $where, $data) {
		$this->db->where($where);
		$this->db->update($table, $data);
	}
}