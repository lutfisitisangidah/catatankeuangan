<div class="container" style="margin-top: 20px;">
	<?php if ($pesan == "berhasilHapus") { ?>
		<div class="alert alert-primary" data-dismiss="alert">Data berhasil dihapus.</div>
	<?php } else if ($pesan == "berhasilTambah") { ?>
		<div class="alert alert-primary" data-dismiss="alert">Data berhasil ditambah.</div>
	<?php } else if ($pesan == "berhasilEdit") { ?>
		<div class="alert alert-primary" data-dismiss="alert">Data berhasil diedit.</div>
	<?php } ?>
	<br>
	<span class="btn btn-success float-end" data-bs-toggle="modal" data-bs-target="#popup">+</span>
	<br><br>
	<table class="table table-striped table-hover">
		<tr>
			<th>No.</th>
			<th>Judul</th>
			<th>Kategori</th>
			<th>Tipe</th>
			<th>Jumlah Barang</th>
			<th>Tanggal</th>
			<th>Hapus</th>
		</tr>
		<?php $no=0;foreach ($transaksi as $t) { ?>
			<?php $no+=1;; ?>
			<tr>
				<td><?= $no ?></td>
				<td><?= $t->judul ?></td>
				<td><?= $t->kategori?></td>
				<td><?= $t->type?></td>
				<td><?= $t->jumlah?></td>
				<td><?= $t->tanggal?></td>
				<td>
					<a class="btn btn-warning "href='<?php echo base_url("catatan/edit?id=".$t->id) ?>'>edit</a> 
					<a class="btn btn-danger"href='<?php echo base_url("catatan/hapus?id=".$t->id) ?>'>hapus</a>
				</td>
			</tr>
		<?php } ?>
	</table>
</div>

<!-- popup -->
<div class="modal fade" id="popup" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Catatan</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<form action="<?php echo base_url() ?>catatan/tambah" method="post">
					<table class="table borderless">
						<tr>
							<th>Judul</th>
							<td><input type="text" class="form-control" name="judul"></td>
						</tr>
						<tr>
							<th>Jumlah</th>
							<td><input type="text" class="form-control" name="jumlah"></td>
						</tr>
						<tr>
							<th>Kategori</th>
							<td>
								<select class="form-select" aria-label="Pilih Kategori" name="kategori">
									<option value="Belanja Umum">Belanja Umum</option>
									<option value="Makanan">Makanan</option>
									<option value="Pulsa">Pulsa</option>
									<option value="Tagihan PDAM">Tagihan PDAM</option>
									<option value="Tagihan Listrik">Tagihan Listrik</option>
									<option value="Transportasix">Transportasi</option>
								</select>
							</td>
						</tr>
						<tr>
							<th>Type</th>
							<td>
								<select class="form-select" aria-label="Pilih Kategori" name="type">
									<option value="Pengeluaran">Pengeluaran</option>
									<option value="Pemasukan">Pemasukan</option>
								</select>
							</td>
						</tr>
						<tr>
							<th>Tanggal</th>
							<td><input type="date" class="form-control" name="tanggal"></td>
						</tr>
					</table>
				</div>
				<div class="modal-footer">
					<span class="btn btn-secondary" data-bs-dismiss="modal">Close</span>
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
		</form>
		</div>
	</div>
</div>