<div class="container">
	<h3>Edit Laporan</h3>
	<form action="<?php echo base_url() ?>catatan/editProses" method="post">
		<table class="table borderless">
			<tr>
				<th>Judul</th>
				<td><input type="text" class="form-control" name="judul" value="<?= $d->judul ?>"></td>
			</tr>
			<tr>
				<th>Jumlah</th>
				<td><input type="text" class="form-control" name="jumlah" value="<?= $d->jumlah ?>"></td>
			</tr>
			<tr>
				<th>Kategori</th>
				<td>
					<select class="form-select" aria-label="Pilih Kategori" name="kategori">
						<option value="Belanja Umum" <?php if ($d->kategori == "Belanja Umum"): ?>
							selected="selected"
						<?php endif ?>>Belanja Umum</option>
						<option value="Makanan" <?php if ($d->kategori == "Makanan"): ?>
							selected="selected"
						<?php endif ?>>Makanan</option>
						<option value="Pulsa" <?php if ($d->kategori == "Pulsa"): ?>
							selected="selected"
						<?php endif ?>>Pulsa</option>
						<option value="Tagihan PDAM" <?php if ($d->kategori == "Tagihan PDAM"): ?>
							selected="selected"
						<?php endif ?>>Tagihan PDAM</option>
						<option value="Tagihan Listrik" <?php if ($d->kategori == "Tagihann Listrik"): ?>
							selected="selected"
						<?php endif ?>>Tagihan Listrik</option>
						<option value="Transportasix" <?php if ($d->kategori == "Transportasi"): ?>
							selected="selected"
						<?php endif ?>>Transportasi</option>
					</select>
				</td>
			</tr>
			<tr>
				<th>Type</th>
				<td>
					<select class="form-select" aria-label="Pilih Kategori" name="type">
						<option value="Pengeluaran">Pengeluaran</option>
						<option value="Pemasukan">Pemasukan</option>
					</select>
				</td>
			</tr>
			<tr>
				<th>Tanggal</th>
				<td><input type="date" class="form-control" value="<?= $d->tanggal ?>" name="tanggal"></td>
			</tr>
		</table>
		<div class="float-end">
			<input type="hidden" value="<?= $d->id ?>" name="id">
			<a href="<?php echo base_url() ?>"><span class="btn btn-secondary" data-bs-dismiss="modal">Batal</span></a>
			<button type="submit" class="btn btn-primary">Edit</button>
		</div>
	</form>
</div>

