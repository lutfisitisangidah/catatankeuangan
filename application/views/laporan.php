<div class="container" style="margin-top: 20px;">
	<table class="table" border="1">
		<tr>
			<th colspan="2">Total</th>
		</tr>
		<tr>
			<td>Jumlah Pemasukan</td>
			<th> Rp. 
			<?= $jmlPengeluaran[0]->jml ?>
			</th>
		</tr>
		<tr>
			<td>Jumlah Pengeluaran</td>
			<th> Rp. 
			<?= $jmlPemasukan[0]->jml ?>
			</th>
		</tr>
	</table>
	<br><br>

	<!-- card laporan -->
	  <div class="card-columns">
	  	<?php foreach ($transaksi as $t): ?>
	  		<div class="card bg-light">
	  		  <div class="card-header">
	  		      <h5 class="card-title"><?= $t->judul ?></h5>
	  		  </div>
	  		  <div class="card-body">
	  		      <table class="table table-borderless">
	  		      	<tr>
	  		      		<td>Kategori</td>
	  		      		<th>: <?= $t->kategori ?></th>
	  		      	</tr>
	  		      </table>
	  		  </div>
	  		</div>
	  	<?php endforeach ?>
	  </div>
	<!-- selesai -->
</div>