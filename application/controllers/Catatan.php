<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catatan extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('model');
	}
	public function index()
	{
		$pesan = '';
		if (isset($_GET['pesan'])) {
			$pesan = $_GET['pesan'];
		}
		$data=[
			'transaksi'=>$this->model->get('transaksi')->result(),
			'halaman' => 'catatan',
			'pesan' => $pesan,
		];
		$this->load->view('header',$data);
		$this->load->view('daftar');
		$this->load->view('footer');
	}
	
	function tambah() {
		$datainput = [
			'judul' => $_POST['judul'],
			'jumlah' => $_POST['jumlah'],
			'kategori' => $_POST['kategori'],
			'type' => $_POST['type'],
			'tanggal' => $_POST['tanggal'],
		];
		

		$this->model->insert_data($datainput, 'transaksi');
		redirect(base_url('catatan?pesan=berhasilTambah'));
	}
	function hapus(){
		$this->model->hapus(['id'=>$_GET['id']],'transaksi');
		redirect(base_url('catatan?pesan=berhasilHapus'));
	}

	function edit() {
		$data=[
			'd' => $this->model->get_satu('transaksi', ['id' => $_GET['id']]),
			'halaman' => 'catatan'
		];
		$this->load->view('header',$data);
		$this->load->view('editCatatan');
		$this->load->view('footer');
	}
	function editProses() {
		$datainput = [
			'judul' => $_POST['judul'],
			'jumlah' => $_POST['jumlah'],
			'kategori' => $_POST['kategori'],
			'type' => $_POST['type'],
			'tanggal' => $_POST['tanggal'],
		];
		

		$this->model->edit_data('transaksi', ['id' => $_POST['id']], $datainput);
		redirect(base_url('catatan?pesan=berhasilEdit'));
	}
}
