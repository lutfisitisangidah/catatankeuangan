<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('model');
	}
	public function index()
	{
		$data=[
			'jmlPengeluaran' => $this->model->sql('SELECT SUM(jumlah) as jml FROM transaksi WHERE type="Pengeluaran"')->result(),
			'jmlPemasukan' => $this->model->sql('SELECT SUM(jumlah) as jml FROM transaksi WHERE type="Pemasukan"')->result(),
			'halaman' => 'laporan',
			'transaksi'=>$this->model->get('transaksi')->result(),
		];
		$this->load->view('header',$data);
		$this->load->view('laporan');
		$this->load->view('footer');
	}

	function tambah() {
		$datainput = [
			'Total' => $_POST['Total'],
			'Top Kategori Pengeluaran' => $_POST['Top Kategori Pengeluaran'],
			'Top Kategori Pemasukan' => $_POST['Top Kategori Pemasukan'],
			'Tampilkan Semua Transaksi' => $_POST['Tampilkan Semua Transaksi'],
			'Tanggal'=> $_POST['Tanggal'],
		];


		$this->model->insert_data($datainput, 'transaksi');
		redirect(base_url('laporan'));
	}
		function hapus(){
			$this->model->hapus(['id'=>$_GET['id']],'transaksi');
			redirect(base_url('laporan'));
		
		}
}